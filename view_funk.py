import json

from flask import Flask, request, Response
from  faker  import  Faker

from db import execute_query, get_customer_names, create_customer
from formatters import format_list
from utils import help_gen_random_users, help_get_avr_data, parse_length, gen_password, parse_chars, get_currency, \
    get_count
import  requests
import os
import csv

fake  =  Faker ()
app = Flask(__name__)


@app.route('/random_users')
def gen_random_users():
	return help_gen_random_users()


@app.route('/requirements')
def get_requirements():
	text=os.path.join(os.getcwd(), "requirements.txt")
	with open(text) as inf:
		return inf.read()


@app.route('/avr_data')
def get_avr_data():
	return help_get_avr_data()



@app.route('/password')
def get_random():
    try:
        length = parse_length(request, 10)
        chars = parse_chars(request, 0)
    except Exception as ex:
        return Response(str(ex), status=400)

    return gen_password(length, chars)

@app.route('/bitcoin')
def get_bitcoin_rate():
    curren = request.args.get('currency', 'USD')
    return get_currency(curren)



@app.route('/unique_names')
def get_unique_names():
    query = 'SELECT COUNT(distinct FirstName) FROM Customers '
    result = execute_query(query)[0][0]
    return f'{result} unique first names is table Customers'


@app.route('/tracks_count')
def get_tracks_count():
    query = 'SELECT   COUNT(distinct TrackID) FROM Tracks '
    result = execute_query(query)[0][0]
    return f'{result} records is table Tracks'

@app.route('/gross_turnover')
def get_gross_turnover():
    query = 'SELECT SUM(UnitPrice * Quantity) FROM Invoice_Items '
    result = execute_query(query)[0][0]
    return f'Total SUM = {round(result,2)}  is table Invoice_Items'


@app.route('/customers')
def get_customers():
    city = request.args.get('city')
    country = request.args.get('country')
    records = get_customer_names(city, country)
    return format_list(records)


@app.route('/genres')
def get_genres():
    querry = """SELECT g.Name, sum(t.Milliseconds)/1000 as Seconds 
             FROM genres g 
             JOIN tracks t 
             on g.GenreId = t.GenreId 
             GROUP by g.name"""
    result = execute_query(querry)
    return format_list(result)


@app.route('/generate_customer')
def generate_customer():
     return create_customer()


@app.route('/generate_customers')
def generate_customers():
    try:
        count = get_count(request)
    except Exception as ex:
        return Response(str(ex), status=400)
    cust_list = []
    for _ in range(count):
        values = create_customer()
        cust_list+=[values]
    return format_list(cust_list)



if __name__ == 'main':
	app.run(port=5001)

#set FLASK_APP=view_funk
#set FLASK_ENV=development
#flask run

