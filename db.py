import os
import sqlite3
from flask import request
from  faker  import  Faker

from formatters import format_list

fake  =  Faker ()




def execute_query(query, *args):
    db_path = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    cur.execute(query, *args)
    conn.commit()
    records = cur.fetchall()
    return records

def get_customer_names(city, country):
    sql_query = 'SELECT FirstName, LastName FROM Customers'
    filters = []
    params = []
    if city or country:
       sql_query += ' WHERE'

    if city:
        filters.append(f' City=?')
        params.append(city)
    if country:

        filters.append(f' Country=?')
        params.append(country)
    if filters:
        sql_query += ' AND'.join(filters)
    #sql_query += ' ORDER BY FirstName'

    records = execute_query(sql_query, params)
    return records


def create_customer():
    customer =f"'{fake.first_name()}', '{fake.last_name()}', '{fake.email()}'"
    querry = f"INSERT INTO Customers ('FirstName', 'LastName', 'Email') \
                                  VALUES ({customer})"
    execute_query(querry)
    return customer







