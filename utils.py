import json
import random
import string

import requests
from flask import Flask, request, Response
from  faker  import  Faker
import os
import csv
fake  =  Faker ()

def help_gen_random_users():
	return ' '.join (f'name: {fake.name()}, email: {fake.email()}' for i in range(100))


def help_get_avr_data():
	exampleFile = open('hw.csv', encoding='UTF-8')
	exampleReader = csv.reader(exampleFile, delimiter=',')
	exampleData = list(exampleReader)
	height=0
	weight=0
	count=0
	inch = float(2.54)
	pounds=float(0.453592)
	for i in exampleData[1:-1]:
		height+=float(i[1])
		weight+=float(i[2])
		count+=1
	exampleFile.close()
	return f'Average height = {round(height*inch/count,2)}cm, average weight = {round(weight*pounds/count,2)}kg'


def parse_length(request, default=10):
    value = request.args.get('length', str(default))

    if not value.isnumeric():
        raise ValueError('Not a number')

    value = int(value)
    if not 3 < value < 100:
        raise ValueError('Out of range')

    return value

def parse_chars(request, default=0):
	chars = request.args.get('chars', str(default))
	if chars in ['1','0']:
		return chars
	else:
		raise ValueError('Out of range')




def gen_password(length, chars):
	if chars == '1':
		length_chars = 2
		length_parol = length - length_chars
		result = ''.join([str(random.randint(0, 9)) for _ in range(length_parol)])  + \
		''.join([str(random.choice(string.ascii_letters)) for _ in range(length_chars)])
	else:
		result = ''.join([str(random.randint(0, 9))for _ in range(length)])
	return result



def get_currency(curren):
	response = requests.get(url='https://bitpay.com/api/rates')
	rates = json.loads(response.text)
	for rate in rates:
		if rate['code'] == str(curren):
			return str(rate['rate'])
	raise ValueError




def get_count(request):
    count = request.args.get('count')
    if not count:
        raise ValueError('Count is required parameter')
    if not count.isnumeric():
        raise ValueError('Not a number')
    count=float(count)

    if not 0 < count <= 100:
        raise ValueError('Out of range')

    return int(count)


